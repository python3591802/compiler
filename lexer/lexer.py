import enum, sys

# Token contains the original text and types of the token
class Token:
    def __init__(self, tokenText, tokenKind):
        self.tokenText = tokenText #Token text. Used for identifiers, strings and numbers
        self.tokenKind = tokenKind # Tokenkind for classification of token

    @staticmethod
    def checkIfKeyword(tokenText):
        for kind in TokenType:
            if kind.name == tokenText and kind.value >= 100 and kind.value < 200:
                return kind
        return None 

#TokenType is an enum for all types of tokens.
class TokenType(enum.Enum):
    
    EOF = -1
    NEWLINE = 0
    NUMBER = 1
    IDENTIFIER = 2
    STRING = 3

    #Keywords
    LABEL = 101
    GOTO = 102
    PRINT = 103
    INPUT = 104
    LET = 105
    IF = 106
    THEN = 107
    ENDIF = 108
    WHILE = 109
    REPEAT = 110
    ENDWHILE = 111

    #Operators
    EQ = 201
    PLUS = 202
    MINUS = 203
    ASTERISK = 204
    SLASH = 205
    EQEQ = 206
    NOTEQ = 207
    LT = 208
    LTEQ = 209
    GT = 210
    GTEQ = 211


class Lexer:

    """ Initialize lexer using source code """
    def __init__(self, source):
        self.source = source + "\n"
        self.currentCharacter = ''
        self.currentPosition = -1
        self.nextChar()

    """ Processes next character """
    def nextChar(self):
        self.currentPosition = self.currentPosition + 1
        if self.currentPosition >= len(self.source):
            self.currentCharacter = '\0' #EOF
        else:
            self.currentCharacter = self.source[self.currentPosition]

    """ Returns the lookahead character """
    def peek(self):
        if self.currentPosition + 1 >= len(self.source):
            return '\0'
        return self.source[self.currentPosition + 1]

    """ Skip the whitespaces, tabs except for newlines. They are used to identify the end of line """
    def skipWhiteSpaces(self):
        while self.currentCharacter == ' ' or self.currentCharacter == '\t' or self.currentCharacter == '\r':
            self.nextChar()
    
    """ skip comments added """
    def skipComments(self):
        if self.currentCharacter == '#':
            while self.currentCharacter != '\n':
                self.nextChar()

    """ Returns the next token """
    def getToken(self):
        self.skipWhiteSpaces()
        self.skipComments()
        token = None
        #Check the first character of this token to see if we can decide what it is.
        #If it a multiple character operator (eg. !=), number, identifier, or keyword we will process the rest.
        if self.currentCharacter == '+':
            token = Token(self.currentCharacter, TokenType.PLUS)
        elif self.currentCharacter == '-':
            token = Token(self.currentCharacter, TokenType.MINUS)
        elif self.currentCharacter == '*':
            token = Token(self.currentCharacter, TokenType.ASTERISK)
        elif self.currentCharacter == '/':
            token = Token(self.currentCharacter, TokenType.SLASH)
        elif self.currentCharacter == '\n':
            token = Token(self.currentCharacter, TokenType.NEWLINE)
        elif self.currentCharacter == '\0':
            token = Token(self.currentCharacter, TokenType.EOF)
        elif self.currentCharacter == '<':
            if self.peek() == '=':
                prevCharacter = self.currentCharacter
                self.nextChar()
                token = Token(prevCharacter + self.currentCharacter, TokenType.GTEQ)
            else:
                token = Token(self.currentCharacter, TokenType.GT)
        elif self.currentCharacter == '>':
            if self.peek() == '=':
                prevCharacter = self.currentCharacter
                self.nextChar()
                token = Token(prevCharacter + self.currentCharacter, TokenType.LTEQ)
            else:
                token = Token(self.currentCharacter, TokenType.LT)
        elif self.currentCharacter == '=':
            if self.peek() == '=':
                prevCharacter = self.currentCharacter
                self.nextChar()
                token = Token(prevCharacter + self.currentCharacter, TokenType.EQEQ)
            else:
                token = Token(self.currentCharacter, TokenType.EQ)
        elif self.currentCharacter == '!':
            if self.peek() == '=':
                prevCharacter = self.currentCharacter
                self.nextChar()
                token = Token(prevCharacter + self.currentCharacter, TokenType.NOTEQ)
            else:
                self.abort("Expected !=, got !" + self.peek())
        #String token generation
        elif self.currentCharacter == '\"':
            self.nextChar()
            startPos = self.currentPosition
            while self.currentCharacter != '\"':
                #Checking for invalid expressions in strings namely for \r, \t, \\, \n and %
                if self.currentCharacter == '\r' or self.currentCharacter == '\t' or self.currentCharacter == '\\' or self.currentCharacter == '%' or self.currentCharacter == '\n':
                    self.abort("Invalid expression added : " + self.currentCharacter)
                self.nextChar()
            token = Token(self.source[startPos: self.currentPosition], TokenType.STRING)
        #Number token generation
        elif self.currentCharacter.isdigit():
            startPos = self.currentPosition
            while self.peek().isdigit():
                self.nextChar()
            if self.currentCharacter == '.': #Decimal part
                if not self.peek().isdigit():
                    self.abort("Illegal decimal format : " + self.peek())
                while self.peek().isdigit():
                    self.nextChar()
            token = Token(self.source[startPos: self.currentPosition + 1], TokenType.NUMBER)
        #Keyword token generation
        elif self.currentCharacter.isalpha():
            #Leading character is alphabetic. So its must be identifier or keyword
            #Get all consecutive alphanumeric characters
            startPos = self.currentPosition
            while self.peek().isalnum():
                self.nextChar()
            
            #Check if word is in current list of keywords
            tokenText = self.source[startPos: self.currentPosition + 1]
            keyword = Token.checkIfKeyword(tokenText)
            if keyword == None: #identifier
                token = Token(tokenText, TokenType.IDENTIFIER)
            else: #keyword
                token = Token(tokenText, keyword)
        else:
            # Unknown error
            self.abort("Unknown Token: " + self.currentCharacter)
        self.nextChar()
        return token

    """ Abort in case of invalid token, display message and exit """
    def abort(self, message):
        sys.exit("Lexer error. " + message)