import unittest, sys

sys.path.append("../")
from lexer import lexer

class TestLexer(unittest.TestCase):

    def test_peek_nextChar(self):
        self.source = "LET feedback = 20"
        self.lex = lexer.Lexer(self.source)
        self.count = 0

        while (self.lex.peek() != '\0'):
            self.count += 1
            self.lex.nextChar()

        self.assertEqual(self.count, len(self.source), "input source code length and characters covered must be equal")

    def test_getToken(self):
        self.source = "+*/-"
        self.lex = lexer.Lexer(self.source)
        self.token = self.lex.getToken()
        self.assertIsInstance(self.token, lexer.Token, "Should be instance of Token")
        self.assertEqual(self.token.tokenKind, lexer.TokenType.PLUS, "Should be plus token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.ASTERISK, "Should be asterisk token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.SLASH, "Should be slash token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.MINUS, "Should be minus token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.NEWLINE, "Should be newline token")

    def test_getToken_WhiteSpaces(self):
        self.source = "+* /-"
        self.lex = lexer.Lexer(self.source)
        self.token = self.lex.getToken()

        self.assertIsInstance(self.token, lexer.Token, "Should be instance of Token")
        self.assertEqual(self.token.tokenKind, lexer.TokenType.PLUS, "Should be plus token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.ASTERISK, "Should be asterisk token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.SLASH, "Should be slash token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.MINUS, "Should be minus token")
        self.token = self.lex.getToken()
        self.assertEqual(self.token.tokenKind, lexer.TokenType.NEWLINE, "Should be newline token")

    def test_getToken_String(self):
        self.source = "+*/- # This is a comment!\n \"This is a string\" "
        self.lex = lexer.Lexer(self.source)
        self.token = self.lex.getToken()
        self.assertIsInstance(self.token, lexer.Token, "Should be instance of Token")

        self.assertEqual(self.token.tokenKind, lexer.TokenType.PLUS, "Should be a plus token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.ASTERISK, "Should be a asterisk token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.SLASH, "Should be a slash token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.MINUS, "Should be a minus token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.NEWLINE, "Should be a newline token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.STRING, "Should be a string token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.NEWLINE, "Should be a newline token")

    def test_getToken_Number(self):
        self.source = "1234 1.234 1234."
        self.lex = lexer.Lexer(self.source)

        self.token = self.lex.getToken()
        self.assertIsInstance(self.token, lexer.Token, "Should be instance of Token")

        self.assertEqual(self.token.tokenKind, lexer.TokenType.NUMBER, "Should be number token")
        self.token = self.lex.getToken()

        self.assertEqual(self.token.tokenKind, lexer.TokenType.NUMBER, "Should be number token")
        
        with self.assertRaises(SystemExit):
            self.lex.getToken()
        
