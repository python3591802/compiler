from lexer import lexer
import sys

# Parser tracks the current token and ensures the code follows the grammar
class Parser:

    def __init__(self, lexer):
        self.lex = lexer
        self.symbols = set() #References for identifiers declared.
        self.labelsDeclared = set() #References for labels declared.
        self.labelsGotoed = set() #References for labels gotoed.
        self.currentToken = None
        self.peekToken = None
        self.nextToken()
        self.nextToken() # called twice to initialize current and peek tokens.

    # Returns true if the current token matches
    def checkToken(self, kind):
        return kind == self.currentToken.tokenKind

    # Return true if the peek token matches
    def checkPeek(self, kind):
        return kind == self.peekToken.tokenKind

    # Matches the currentToken. If not match, abort. Advance to next token
    def matchToken(self, kind):
        if not self.checkToken(kind):
            self.abort("Expected : " + kind.name + ", got " + self.currentToken.tokenKind.name)
        self.nextToken()

    # Get the next token
    def nextToken(self):
        self.currentToken = self.peekToken
        self.peekToken = self.lex.getToken()

    def abort(self, message):
        sys.exit("Error. " + message)
    
    # program processing
    def program(self):
        # program :: {statement}
        print("PROGRAM")

        # Handline newlines added at start of the program
        while self.checkToken(lexer.TokenType.NEWLINE):
            self.nextToken()

        # Process all the statements in the program
        while not self.checkToken(lexer.TokenType.EOF):
            self.statement()
        
        # Checking whether GOTO statement is referencing an undeclared label.
        for label in self.labelsDeclared:
            if label not in self.labelsGotoed:
                self.abort("Goto statement to undeclared label : " + label)

    # statement processing
    def statement(self):
        # Check the first keyword to see the type of statement to Process

        if self.checkToken(lexer.TokenType.PRINT):
            # statement ::= "PRINT" (expression | string) nl
            print("STATEMENT-PRINT")
            self.nextToken()

            if self.checkToken(lexer.TokenType.STRING):
                # String token
                self.nextToken()
            else:
                # expression
                self.expression()

        elif self.checkToken(lexer.TokenType.IF):
            # statement ::= "IF" comparison "THEN" nl {statement} "ENDIF" nl
            print("STATEMENT-IF")
            self.nextToken()
            self.comparison()

            self.matchToken(lexer.TokenType.THEN)
            self.nl()

            while not self.checkToken(lexer.TokenType.ENDIF):
                self.statement()

            self.matchToken(lexer.TokenType.ENDIF)
        
        elif self.checkToken(lexer.TokenType.WHILE):
            # statement ::= "WHILE" comparison "REPEAT" nl {statement} "ENDWHILE" nl
            print("STATEMENT-WHILE")
            self.nextToken()
            self.comparison()

            self.matchToken(lexer.TokenType.REPEAT)
            self.nl()

            while not self.checkToken(lexer.TokenType.ENDWHILE):
                self.statement()

            self.matchToken(lexer.TokenType.ENDWHILE)

        elif self.checkToken(lexer.TokenType.LABEL):
            # statement ::= "LABEL" identifier nl
            print("STATEMENT-LABEL")
            self.nextToken()

            if self.currentToken.tokenText in self.labelsDeclared:
                self.abort("Label already exists : " + self.currentToken.tokenText)
            self.labelsDeclared.add(self.currentToken.tokenText)

            self.matchToken(lexer.TokenType.IDENTIFIER)

        elif self.checkToken(lexer.TokenType.GOTO):
            # statement ::= "GOTO" identifier nl
            print("STATEMENT-GOTO")
            self.nextToken()
            self.labelsGotoed.add(self.currentToken.tokenText)
            self.matchToken(lexer.TokenType.IDENTIFIER)

        elif self.checkToken(lexer.TokenType.LET):
            # statement ::= "LET" IDENTIFIER = expression nl
            print("STATEMENT-LET")
            self.nextToken()

            # Check if identifier is not in symbols declared set then add it.
            if self.currentToken.tokenText not in self.symbols:
                self.symbols.add(self.currentToken.tokenText)

            self.matchToken(lexer.TokenType.IDENTIFIER)
            self.matchToken(lexer.TokenType.EQ)
            self.expression()

        elif self.checkToken(lexer.TokenType.INPUT):
            # statement ::= "INPUT" IDENTIFIER nl
            print("STATEMENT-INPUT")
            self.nextToken()
            
            # Check if identifier is not in symbols declared set then add it.
            if self.currentToken.tokenText not in self.symbols:
                self.symbols.add(self.currentToken.tokenText)

            self.matchToken(lexer.TokenType.IDENTIFIER)

        else:
            self.abort("Invalid statement at " + self.currentToken.tokenText + " of type " + self.currentToken.tokenKind.name)
        
        # Newline
        self.nl()

    def comparison(self):
        # comparison ::= expression (("==" | "!=" | "<" | "<=" | ">=" | ">") expression)+
        print("COMPARISON")

        self.expression()

        # Atleast one more expression must be present for comparison
        if self.checkComparisonOperator():
            self.nextToken()
            self.expression()
        else:
            self.abort("Expected comparison operator at " + self.currentToken.tokenText)

        # Can be more than one expression's for comparison
        while self.checkComparisonOperator():
            self.nextToken()
            self.expression()
    def checkComparisonOperator(self):
        return self.checkToken(lexer.TokenType.EQEQ) or self.checkToken(lexer.TokenType.NOTEQ) or self.checkToken(lexer.TokenType.LT) or self.checkToken(lexer.TokenType.LTEQ) or self.checkToken(lexer.TokenType.GTEQ) or self.checkToken(lexer.TokenType.GT)

    def expression(self):
        # expression ::= term {("+" | "-") term}
        print("EXPRESSION")

        self.term()
        # if more than one term components are present
        while self.checkToken(lexer.TokenType.PLUS) or self.checkToken(lexer.TokenType.MINUS):
            self.nextToken()
            self.term()

    def term(self):
        # term ::= unary {("/" | "*") unary}
        print("TERM")

        self.unary()
        # if more than one unary terms are present
        while self.checkToken(lexer.TokenType.SLASH) or self.checkToken(lexer.TokenType.ASTERISK):
            self.nextToken()
            self.unary()

    def unary(self):
        # unary ::= ["+"/"-"] primary
        print("UNARY")

        if self.checkToken(lexer.TokenType.PLUS) or self.checkToken(lexer.TokenType.MINUS):
            self.nextToken()
        self.primary()

    def primary(self):
        # primary ::= IDENTIFIER | NUMBER
        print("PRIMARY ( " + self.currentToken.tokenText + " )");

        if self.checkToken(lexer.TokenType.IDENTIFIER):
            # Checking if identifier is declared or not
            if self.currentToken.tokenText not in self.symbols:
                self.abort("Attempting to access undeclared identifier : " + self.currentToken.tokenText)
            self.nextToken()
        elif self.checkToken(lexer.TokenType.NUMBER):
            self.nextToken()
        else:
            #Error
            self.abort("Expected IDENTIFIER or NUMBER got : " + self.currentToken.tokenText)

        
    # newline processing
    # nl :: "\n"+
    def nl(self):
        print("NEWLINE")

        # Atleast one newline token should be present
        self.matchToken(lexer.TokenType.NEWLINE)
        # Multiple newlines are allowed to be added
        while self.checkToken(lexer.TokenType.NEWLINE):
            self.nextToken()
