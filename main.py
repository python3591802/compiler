from lexer import lexer
from parser import parser
import sys

def main():

    print("Teeny Tiny Compiler")

    if len(sys.argv) != 2:
        sys.exit("Compiler needs the source code file")
    with open(sys.argv[1], 'r') as inputFile:
        source = inputFile.read()
    
    lex = lexer.Lexer(source)
    pars = parser.Parser(lex)

    pars.program()
    print("Parsing completed successfully")

main()
